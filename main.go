package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/metric/global"
	"go.opentelemetry.io/otel/metric/instrument"
	"go.opentelemetry.io/otel/metric/instrument/syncint64"
	controller "go.opentelemetry.io/otel/sdk/metric/controller/basic"
	processor "go.opentelemetry.io/otel/sdk/metric/processor/basic"
	"go.opentelemetry.io/otel/sdk/metric/selector/simple"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
)

var (
	otelAgentAddr = "localhost:4318"
	tracer        = otel.Tracer("otelPOC")
	requestCount  syncint64.Counter
)

func main() {
	ctx := context.Background()

	metricClient := otlpmetrichttp.NewClient(
		otlpmetrichttp.WithInsecure(),
		otlpmetrichttp.WithEndpoint(otelAgentAddr))

	metricExp, err := otlpmetric.New(ctx, metricClient)
	if err != nil {
		log.Fatal(err)
	}

	pusher := controller.New(
		processor.NewFactory(
			simple.NewWithHistogramDistribution(),
			metricExp,
		),
		controller.WithExporter(metricExp),
		controller.WithCollectPeriod(2*time.Second),
	)
	global.SetMeterProvider(pusher)

	err = pusher.Start(ctx)
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Create("traces.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	traceClient := otlptracehttp.NewClient(
		otlptracehttp.WithInsecure(),
		otlptracehttp.WithEndpoint(otelAgentAddr))

	traceOtelExp, err := otlptrace.New(ctx, traceClient)
	if err != nil {
		log.Fatal(err)
	}

	traceStandardOutExp, err := stdouttrace.New(
		stdouttrace.WithWriter(f),
		stdouttrace.WithPrettyPrint(),
		stdouttrace.WithoutTimestamps(),
	)
	if err != nil {
		log.Fatal(err)
	}

	tp := trace.NewTracerProvider(
		trace.WithBatcher(traceOtelExp),
		trace.WithBatcher(traceStandardOutExp),
		trace.WithResource(newResource()),
	)

	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Fatal(err)
		}
	}()

	otel.SetTracerProvider(tp)

	meter := global.Meter("otel-POC-meter")
	requestCount, err = meter.SyncInt64().Counter(
		"otel_POC/request_counts",
		instrument.WithDescription("The number of requests received"),
	)
	if err != nil {
		log.Print(err)
	}

	// Wrap your httpHandler function.
	handler := http.HandlerFunc(httpHandler)
	wrappedHandler := otelhttp.NewHandler(handler, "top")
	http.Handle("/hello", wrappedHandler)

	// And start the HTTP serve.
	fmt.Println("listining")
	log.Fatal(http.ListenAndServe(":3012", nil))
}

// newResource returns a resource describing this application.
func newResource() *resource.Resource {
	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("otelPOC"),
			semconv.ServiceVersionKey.String("v0.1.0"),
			attribute.String("environment", "demo"),
		),
	)
	return r
}

func httpHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	requestCount.Add(ctx, 1)
	first(ctx)
	second(ctx)
	third(ctx)
}

func first(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "first")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("first.duration", int(sleepTime)))
	firstSub1(ctx2)
}

func firstSub1(ctx context.Context) {
	_, span := tracer.Start(ctx, "firstSub1")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("firstSub1.duration", int(sleepTime)))
}

func second(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "second")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("second.duration", int(sleepTime)))
	secondSub1(ctx2)
}

func secondSub1(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "secondSub1")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("secondSub1.duration", int(sleepTime)))
	secondSub2(ctx2)
}

func secondSub2(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "secondSub2")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("secondSub2.duration", int(sleepTime)))
	secondSub3(ctx2)
}

func secondSub3(ctx context.Context) {
	_, span := tracer.Start(ctx, "secondSub3")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("secondSub3.duration", int(sleepTime)))
}

func third(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "third")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond
	span.SetAttributes(attribute.Int("third.duration", int(sleepTime)))
	thirdSub1(ctx2)
}

func thirdSub1(ctx context.Context) {
	ctx2, span := tracer.Start(ctx, "thirdSub1")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond

	span.SetAttributes(attribute.Int("thirdSub1.duration", int(sleepTime)))
	thirdSub2(ctx2)
}

func thirdSub2(ctx context.Context) {
	_, span := tracer.Start(ctx, "thirdSub2")
	defer span.End()

	sleepTime := time.Duration(rand.Intn(1000)) * time.Millisecond
	span.SetAttributes(attribute.Int("thirdSub2.duration", int(sleepTime)))
}
